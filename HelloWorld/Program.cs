﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld
{
    class Program
    {
        static void Main(string[] args)
        {
            string s = "Hello World!";

            for (int i = 0; i < s.Length; i++)
                Console.WriteLine(s[i]);

            Console.WriteLine("\n====================\n");

            for (int i = s.Length - 1; i >= 0; i--)
                Console.WriteLine(s[i]);

            Console.WriteLine("\n====================\n");

            Console.WriteLine(s);

            Console.ReadKey();
        }
    }
}
